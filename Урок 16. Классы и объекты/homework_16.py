from math import *

# Задание №1
#
# Создайте класс Касса, который хранит текущее количество денег в кассе, у него есть методы:
#
# top_up(X) - пополнить на X
# count_1000() - выводит сколько целых тысяч осталось в кассе
# take_away(X) - забрать X из кассы, либо выкинуть ошибку, что не достаточно денег

class Cashbox(object):
    def __init__(self, cash):
        self.cash = cash

    def top_up(self, x):
        self.cash += x
        print('Деньги зачислены. В кассе осталось: ', self.cash)

    def take_away(self, x):
        if self.cash >= x:
            self.cash -= x
            print('Деньги выданы. В кассе осталось: ', self.cash)
        else:
            print('В кассе недостаточно средств! ', self.cash)

    def count_1000(self):
        print(self.cash // 1000, '- целых тысяч осталось в кассе')


def exercise_1():
    cash = Cashbox(5000)
    cash.top_up(500)
    cash.take_away(1700)
    cash.count_1000()


# Задание №2
#
# Создайте класс Черепашка, который хранит позиции x и y черепашки, а также s - количество клеточек, на которое она
# перемещается за ход
#
# у этого класс есть методы:
#
# go_up() - увеличивает y на s
# go_down() - уменьшает y на s
# go_left() - уменьшает x на s
# go_right() - увеличивает y на s
# evolve() - увеличивает s на 1
# degrade() - уменьшает s на 1 или выкидывает ошибку, когда s может стать ≤ 0
# count_moves(x2, y2) - возвращает минимальное количество действий, за которое черепашка сможет добраться до x2 y2 от
# текущей позиции

class Tortle(object):
    def __init__(self, x, y, s):
        self.x = x
        self.y = y
        self.s = s

    def go_up(self):
        self.y += self.s

    def go_down(self):
        self.y -= self.s

    def go_left(self):
        self.x -= self.s

    def go_right(self):
        self.x += self.s

    def evolve(self):
        self.s += 1

    def degrade(self):
        if self.s == 1:
            print('s некуда больше уменьшать')
        else:
            self.s -= 1

    def count_moves(self, x2, y2):
        dx, dy = abs(self.x - x2), abs(self.y - y2)
        cnt = dx // self.s + dy // self.s
        if dx % self.s != 0:
            cnt += dx % self.s + 1
        if dy % self.s != 0:
            cnt += dy % self.s + 1
        print(f'минимальное количество действий, за которое черепашка сможет добраться до {x2} {y2} от текущей позиции:'
              f' {cnt}')


def exercise_2():
    tortle = Tortle(5, 5, 2)
    tortle.count_moves(-2, 7)


exercise_1()
print()
exercise_2()
